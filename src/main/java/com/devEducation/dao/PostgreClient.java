package com.devEducation.dao;

import java.security.Provider;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

public class PostgreClient {
    private static Statement stmt;
    private static PreparedStatement preparedStatement;
    private static ResultSet rs;
    private static Connection connection = null;

    public PostgreClient() {
        System.out.println("-------- PostgreSQL "
                + "JDBC Connection Testing ------------");
        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();

        }
        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://ec2-3-248-4-172.eu-west-1.compute.amazonaws.com:5432/d53knvbfufupa3", "gnqzinvidwoiqa",
                    "3b966ca1a99d7a2f0e97131be7bd08862b343ef02467ced66ad98e855076db34");
            stmt = connection.createStatement();
        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();


        }
        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }

        try {
            String myTableName = "DROP TABLE number2";
            stmt.executeUpdate(myTableName);

        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        }



        try {

            String myTableName = "CREATE TABLE number2 ("
                    + "id   SERIAL PRIMARY KEY ,"
                    + "num  INTEGER NOT NULL);";
            stmt.executeUpdate(myTableName);

        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        }

    }



    public Boolean pushPersonToDatabase(Integer num, Integer i) {

        try {
            String query = "INSERT INTO number2 (id,num) VALUES (?,?)";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, i);
            preparedStatement.setInt(2, num);
            preparedStatement.executeUpdate();

            return true;

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
            return false;
        }
    }

    public static List<Integer> readFromDatabase() {
        List<Integer> list = new ArrayList<>();
        try {

            String query = "select num from number2 ";

            rs = stmt.executeQuery(query);
            while (rs.next()) {

                int num = parseInt(rs.getString("num"));
                list.add(num);

            }

            return list;

        } catch (SQLException throwables) {
            System.out.println("loc");
            throwables.printStackTrace();
            return null;
        }

    }




}
