package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.swing.table.DefaultTableModel;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;



public class Controller implements Initializable {

    private ObservableList<ObservableList> data;
    public Button getData;

    @FXML
    public TableView<String> table;

    @FXML
    private TableColumn<String, String> columnNumbers;

    @FXML
    public void initialize() {
        prepareTable();
    }

    @FXML
    private Button connect;

    @FXML
    void action(ActionEvent event) throws IOException {
        setCommandStart();


    }

    @FXML
    void actionget(ActionEvent event) throws IOException {
        setCommandGet();

    }

    public void setCommandStart() throws IOException {
        Socket socket = new Socket();
        socket.setCommandStart();
        List list = new ArrayList();
        int count = 0;
        while (count != 101) {

            System.out.println(Socket.getDataSocket1());

            table.getItems().add(Socket.getDataSocket1());

            count++;
        }
        System.out.println(list);

    }

    public void setCommandGet() throws IOException {
        Socket socket = new Socket();
        socket.setCommandStart2();
        socket.getDataSocket();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    private void prepareTable() {
        columnNumbers.setCellValueFactory(new PropertyValueFactory<String, String>("value"));
    }
}
