package sample;

import javafx.scene.control.TableView;
import javafx.stage.FileChooser;

import javax.swing.text.TabableView;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Socket {

    private static SocketChannel channel;
    private static SocketAddress socketAddress;

    private static ByteBuffer byteBuffer;

    private static final String HOST = "localhost";
    private static final int PORT = 9000;

    public void setCommandStart() throws IOException {
        channel = SocketChannel.open();
        socketAddress = new InetSocketAddress(HOST, PORT);
        if (!channel.isConnected()) {
            channel.connect(socketAddress);

        }
        String result = "H";
        byteBuffer = ByteBuffer.allocate(1024);
        byteBuffer.put(result.getBytes());
        byteBuffer.flip();

        while (byteBuffer.hasRemaining()) {
            channel.write(byteBuffer);
        }
        byteBuffer.clear();


    }

    public void setCommandStart2() throws IOException {
        channel = SocketChannel.open();
        socketAddress = new InetSocketAddress("localhost", 9000);

        if (!channel.isConnected()) {
            channel.connect(socketAddress);
        }
        String result = "G";
        byteBuffer = ByteBuffer.allocate(1024);
        byteBuffer.clear();
        byteBuffer.put(result.getBytes());
        byteBuffer.flip();

        while (byteBuffer.hasRemaining()) {
            channel.write(byteBuffer);
        }
    }

    public void getDataSocket() {

        String command = null;
        try {
            channel.read(byteBuffer);
            byteBuffer.flip();
            command = new String(byteBuffer.array(), StandardCharsets.UTF_8);
            byteBuffer.clear();
            System.out.println(command);


            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save file");
            File dest = fileChooser.showSaveDialog(null);
            if (dest != null) {
                try {
                    Files.copy(Paths.get(command), dest.toPath());
                } catch (IOException ex) {

                    System.out.println(ex.getMessage());
                }
            }

        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }


    public static String getDataSocket1() {

        String command = null;
        try {
                channel.read(byteBuffer);
                byteBuffer.flip();

                command = new String(byteBuffer.array(), StandardCharsets.UTF_8);

                byteBuffer.clear();

        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        }
        return command;
    }

}
