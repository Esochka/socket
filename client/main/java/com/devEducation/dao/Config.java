package com.devEducation.dao;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Config {

    public static File toCsv(List<Integer> nums) {
        File file = new File("cv.csv");

        StringBuffer sb = null;
        if (nums.isEmpty()) {
            sb = new StringBuffer("");
        } else {
            sb = new StringBuffer("number \n");
        }

        for (Integer integer : nums
        ) {
            sb.append((integer)).append(" \n");
        }
        try (FileWriter writer = new FileWriter(file, true)) {
            writer.write(String.valueOf(sb));
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return file;
    }
}
