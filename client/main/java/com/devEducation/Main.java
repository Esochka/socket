package com.devEducation;

import com.devEducation.dao.Config;
import com.devEducation.dao.PostgreClient;
import com.devEducation.model.Generator;
import com.devEducation.socket.ServerSocket;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocket socket = new ServerSocket();
        SocketChannel client = null;


        if (client == null) {
            client = socket.initialize(9000);
        }
        ByteBuffer byteBuffer = socket.getByteBuffer();
        StringBuffer text = new StringBuffer("");
        String command = " ";
        command = GetCom(client, byteBuffer);
        byteBuffer.flip();


            if (command.startsWith("H")) {
                Start(client, byteBuffer);
                while (command.startsWith("H")){
                    command = GetCom(client, byteBuffer);
                }




            }
            if (command.startsWith("G")) {
                Get(client);

            }
    }

    public static void Start(SocketChannel client, ByteBuffer byteBuffer) throws InterruptedException, IOException {

        for (int i = 0; i < 100; i++) {
            int value = Generator.generate(i);
            client.write(ByteBuffer.wrap(String.valueOf(value).getBytes(StandardCharsets.UTF_8)));
            byteBuffer.clear();
            byteBuffer.flip();
        }
        byteBuffer.clear();
        byteBuffer.flip();
    }

    public static void Get(SocketChannel client) throws IOException {

        List<Integer> list = PostgreClient.readFromDatabase();
        Config.toCsv(list);
        client.write(ByteBuffer.wrap(Config.toCsv(list).getCanonicalPath().replaceAll("\\\\", "/").getBytes(StandardCharsets.UTF_8)));
    }

    public static String GetCom(SocketChannel client, ByteBuffer byteBuffer) throws IOException {
        client.read(byteBuffer);
        byteBuffer.flip();
        String command = new String(byteBuffer.array(), "UTF-8");
        System.out.println(command);
        byteBuffer.clear();
        return command;
    }
}
